This repository originally contains the solution to be implemented on Docker. A procedure has been developed to migrate Docker compose files to Kubernetes, according to the link given below.

Translate a Docker Compose File to Kubernetes Resources
https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/

For this you should have a working Kubernetes system, and in the lab setup I had a MicroK8s implementation.

Please refer to the links below to implement MicroK8s.
https://microk8s.io
https://ubuntu.com/tutorials/install-a-local-kubernetes-with-microk8s


Accordngly, below given commands were run to translate our Docker solution into Kubernetes.

Step 1 - Clone the repo and change into dir
linux@ubuntu:/opt$ git clone https://gitlab.com/manish.gvgn/php-mysql-mongo-ssl-cicd
linux@ubuntu:/opt$ cd php-mysql-mongo-ssl-cicd/

Step 2 - Install Kompose
sudo curl -L https://github.com/kubernetes/kompose/releases/download/v1.21.0/kompose-linux-amd64 -o kompose
linux@ubuntu:/opt/php-mysql-mongo-ssl-cicd$ sudo chmod +x kompose
linux@ubuntu:/opt/php-mysql-mongo-ssl-cicd$ sudo mv ./kompose /usr/local/bin/kompose

Step 3 - Convert docker-compose file into kubernetes yml
linux@ubuntu:/opt/php-mysql-mongo-ssl-cicd$ sudo kompose convert

Step 4 - Run Kubernetes pods now
linux@ubuntu:/opt/php-mysql-mongo-ssl-cicd$ sudo microk8s kubectl apply -f mysql-deployment.yaml,mongo-deployment.yaml,mongo-express-deployment.yaml,mongo-express-service.yaml,php-claim0-persistentvolumeclaim.yaml,php-deployment.yaml,phpmyadmin-claim0-persistentvolumeclaim.yaml,phpmyadmin-deployment.yaml,phpmyadmin-service.yaml,php-service.yaml

Step 5 - View details of Kubernetes Pods
linux@ubuntu:/opt/php-mysql-mongo-ssl-cicd$ sudo microk8s kubectl get all --all-namespaces

From the output, determine the IP addresses of services and access them according to their description given in the Readme file.