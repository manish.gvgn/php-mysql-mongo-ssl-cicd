<?php

   echo "This is a PHP demo application for checking MySQL and MongoDB database connectivity.";
   echo "<br><br>";

// Code for MySQL Database Connectivity

   echo "Section 1 - Testing MySQL database connectivity.";
   echo "<br><br>";


if(!empty($_ENV['MYSQL_HOST']))
   $host = $_ENV['MYSQL_HOST'];
else
   $host = 'moe-mysql-app';

if(!empty($_ENV['MYSQL_USER']))
   $user = $_ENV['MYSQL_USER'];
else
   $user = 'moeuser';

if(!empty($_ENV['MYSQL_PASSWORD']))
   $pass = $_ENV['MYSQL_PASSWORD'];
else
   $pass = 'moepass';

if(!empty($_ENV['MYSQL_DB']))
   $db_name = $_ENV['MYSQL_DB'];
else
   $db_name = 'moe_db';

echo "Connecting to Database: $host $user $pass $db_name";
echo "<br><br>";

$conn = new mysqli($host, $user, $pass, $db_name);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected to MySQL successfully!";
echo "<br><br>";

$conn->close();


// Code for Mongo Database Connectivity

    echo "Section 2 - Testing MongoDB database connectivity.";
    echo "<br><br>";

try {

    $mng = new MongoDB\Driver\Manager("mongodb://root:example@moe-mongo:27017");

    $stats = new MongoDB\Driver\Command(["dbstats" => 1]);
    $res = $mng->executeCommand("local", $stats);

    $stats = current($res->toArray());

    print_r($stats);
    echo "<br><br>";

    echo "Connected to MongoDB successfully!";
    echo "<br><br>";

} catch (MongoDB\Driver\Exception\Exception $e) {

    $filename = basename(__FILE__);

    echo "The $filename script has experienced an error.\n";
    echo "It failed with the following exception:\n";

    echo "Exception:", $e->getMessage(), "\n";
    echo "In file:", $e->getFile(), "\n";
    echo "On line:", $e->getLine(), "\n";
}


?>
