# PHP MySQL Mongo SSL CICD

PHP MySQL Mongo applications on docker containers with SSL and CICD pipelines.

This repository is a set of Docker containers for the following services:

- A MySQL database service
- A Mongo database service
- Apache Web Server with PHP support for both the databases

This solution can be accessed in following ways:
1. git clone git@gitlab.com:manish.gvgn/php-mysql-mongo-ssl-cicd.git
2. Register or Login to gitlab.com and fork the above repository, and then run the CI CD pipeline

To run the applications:
- Install Docker by referng to this link https://www.docker.com/get-started
- Follow the example for Linux as show below

On Linux
```
[root@centos7]# cd /opt
[root@centos7 opt]# git clone git@gitlab.com:manish.gvgn/php-mysql-mongo-ssl-cicd.git
[root@centos7 php-mysql-mongo-ssl-cicd]# docker-compose up -d
```


This will start 5 Docker containers as programmed in the docker-compose.yml file.

To access and test the applications:

```
Web & PHP  : http://Docker-Host-IP and https://Docker-Host-IP

phpMyAdmin : http://Docker-Host-IP:30002 and https://Docker-Host-IP:30002

Mongo Express : http://Docker-Host-IP:8081 and https://Docker-Host-IP:8081
```



The Web Server contains a script in the "index.php" file that connects to both the databases and shows if the connection was successful or unsuccessful.
